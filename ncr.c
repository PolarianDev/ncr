#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#ifndef INFINITY
#define INFINITY 1.0 / 0.0
#endif

#ifndef NAN
#define NAN 0.0/0.0
#endif

long factorial(unsigned int num) {
    if (num < 0) {
        return NAN;
    }
    if (INFINITY < num) {
        return INFINITY;
    }
    unsigned long result = 1;
    for (unsigned int i = 1; i < num + 1; i++) {
        if (result > ULONG_MAX) {
            return INFINITY;
        }
        result *= i;
    }
    return result;
}

// TODO: rewrite ncr to handle NAN and INFINITY
long ncr(unsigned long n, unsigned int r) {
    return (unsigned long)factorial(n) / (factorial(r) * factorial(n - r));
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        puts("Insufficent parameters");
        return 1;
    }
    printf("ncr: %ld\n", ncr(strtol(argv[1], NULL, 10), strtol(argv[2], NULL, 10)));
    return 0;
}