# ncr calculator
A simple program to calculate ncr

# Usage
- Clone this repo: `git clone https://gitlab.com/PolarianDev/ncr`
- Build the code: `./build`
- Run the code, replacing <> with the values: `./ncr <n> <r>`